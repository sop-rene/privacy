Folgende Datenschutzerklärungen decken diverse Services ab:

* https://opr.vc/docs/fonts/font_awesome/
* https://www.dsm-online.eu/datenschutzerklaerung/ (Google, Wordpress, Cookiebot, FA)
* https://trockenbau-breisgau.de/datenschutz/ (GFonts, FA, GMaps, )
* https://fixinggroup.com/datenschutzerklaerung (GAnalytics, GFonts, FA, YT)