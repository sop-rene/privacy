# Datenschutz

## Problem

Cookies und anderes Tracking von personenbezogenen Daten soll ohne Einverständnis vermieden werden. Dazu zählen unter anderem Dienstleistungen von:

* Google Fonts
* Google Analytics
* Google Tagmanager
* Google Maps
* YouTube
* FontAwesome
* CDN

## Lösung

### 1. Externe Dateien lokal einbinden

Dateien lokal zu hosten verhindert in manchen Fällen, dass Daten auf fremden Servern verarbeitet werden.  Manchmal ist dies jedoch nicht möglich und daher nur eine halbe Lösung.

Funktioniert für:

* Google Fonts
* FontAwesome
* Skripte (ohne dependencies)
* Bilder

Funktioniert nicht für:

* Google Analytics
* Google Tagmanager

#### Vorteile
* Unabhängig von Drittanbietern

#### Nachteile
* Verbot bestimmte Dokumente selbst zu hosten
* Google Maps selbst hosten wird vermutlich nicht funktioniern


### 2. Einverständnis proaktiv einholen (Consent Tool)

Über ein Consent Tool wird abgefragt, welche Services geladen werden sollen und welche nicht. Dazu werden im Vorfeld alle relevanten Abschnitte auf der Webseite mit Platzhaltern bestückt und erst nach Bestätigung freigeschalten.

Ein funktionierendes Beispiel für Google Fonts findet sich auf: https://rene-test.sop-werft.de/.

Funktioniert für:

* Google Fonts
* Google Analytics
* FontAwesome
* CDNs (keine Dependencies)

Funktioniert nicht für:

* Google Tagmanager (?)


#### Vorteile
* Updates werden automatisch eingespielt

#### Nachteile


### 3. Auf Datenschutzerklärung verweisen

In einigen Fällen *könnte* es auch reichen auf den bestimmten Abschnitt in der DSGVO zu verweisen. So steh beispielsweise im **Art. 6 Abs. 1 lit. f DSGVO** geschrieben, dass wenn berechtigtes Interesse besteht, Cookies gesetzt werden dürfen.

#### Vorteile
* Kein technischer Aufwand

#### Nachteile
* setzt hohes Maß an Rechtsverständnis voraus