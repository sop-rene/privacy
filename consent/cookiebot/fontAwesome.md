wie google fonts mit folgenden ergänzungen:

* **Variante A**: über CSS Pseudo-Klasse
* **Variante B**: über einen Platzhaltetext im HTML

head:
```html
<style>
    [class*="fa-"] {
        font-style: normal;
    }
    [class*="fa-"]::before {
        content: "▯";
    }
</style>

<script type="text/plain" data-cookieconsent="marketing">
    !function() {
        /**
        * FontAwesome
        */

        // remove placeholder general `▯`
        const faPlaceholderStyle = document.head.appendChild(document.createElement("style"));
        faPlaceholderStyle.innerHTML = "[class*='fa-']:before {content: '';}";

        // remove placeholder custom `fa_face_awesome`
        //const faPlaceholder = [...document.querySelectorAll('[class*="fa-"]')];
        //faPlaceholder.forEach(p => p.textContent = '');

        const fa = document.createElement("script");
        fa.src = 'https://kit.fontawesome.com/9ce15be154.js';
        fa.crossorigin = "anonymous";
        document.head.appendChild(fa);
    }()
</script>
```

body:
```html
<i class="fa-thin fa-face-awesome huge"></i>
```